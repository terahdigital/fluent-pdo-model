<?php declare(strict_types=1);

namespace Terah\FluentPdoModel;

class Column
{

    public string $tableName    = '';

    public string $columnName   = '';

    public bool $isNullable     = false;

    public string $dataType     = '';

    public int $maxLength       = 0;

    public int $precision       = 0;

    public string $columnType   = '';

    public string $comment      = '';

    /** @var string[] */
    public array $attrs         = [];

    /** @var string[] */
    public array $options       = [];
}