<?php declare(strict_types=1);
/**
 * @package Terah\FluentPdoModel
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace Terah\FluentPdoModel;

use Closure;
use Psr\Log\LoggerInterface;
use Terah\FluentPdoModel\Drivers\AbstractPdo;
use Terah\FluentPdoModel\Drivers\DriverInterface;
use Terah\RedisCache\CacheInterface;
use Terah\Assert\Assert;

class ConnectionPool
{
    /** @var DriverInterface[] */
    protected array $_connections               = [];

    /** @var array|Closure[] */
    protected array $_setups                    = [];

    static protected ?ConnectionPool $instance  = null;


    protected function __construct()
    {}


    public function addConnections(array $connections=[], LoggerInterface $logger=null, CacheInterface $cache=null)
    {
        foreach ( $connections as $name => $conn )
        {
            $this->addConnection($name, $conn, $logger, $cache);
        }
    }


    public static function getDriverFromArray(array $config, LoggerInterface $logger=null, CacheInterface $cache=null) : Closure
    {
        $dsn                    = $user = $pass = '';
        extract($config);
        unset($config['dsn'],$config['user'], $config['pass']);
        Assert::that($dsn)->notEmpty("The dsn cannot be empty.");
        $driver                 = 'Terah\\FluentPdoModel\\Drivers\\' . ucfirst(strtolower(substr($dsn, 0, strpos($dsn, ':')))) . 'Pdo';
        Assert::that($driver)->classExists("The database driver {$driver} is not supported.");

        return function() use ($driver, $dsn, $user, $pass, $config, $logger, $cache) {

            return new $driver($dsn, $user, $pass, $config, $logger, $cache);
        };
    }


    /**
     * @param string $name
     * @param DriverInterface|array|Closure $conn
     * @param LoggerInterface|null $logger
     * @param CacheInterface|null $cache
     * @return $this
     */
    public function addConnection(string $name, $conn, LoggerInterface $logger=null, CacheInterface $cache=null) : ConnectionPool
    {
        Assert::that($name)->notEmpty("The connection name cannot be empty.");
        Assert::that($conn)->notEmpty("The connection details cannot be empty.");

        if ( $conn instanceof AbstractPdo || $conn instanceof Closure )
        {
            $this->_setups[$name]   = $conn;

            return $this;
        }
        $this->_setups[$name]   = static::getDriverFromArray($conn, $logger, $cache);

        return $this;
    }


    public static function add(string $name, AbstractPdo $conn) : ConnectionPool
    {
        return static::getInstance()->addConnection($name, $conn);
    }


    public function getConnection(string $name, bool $reconnect=false) : AbstractPdo
    {
        Assert::that($this->_setups)->keyExists($name, "The database connection {$name} has not been added.");

        if ( $reconnect )
        {
            unset($this->_connections[$name]);
        }
        if ( isset($this->_connections[$name]) && $this->_connections[$name] instanceof AbstractPdo )
        {
            return $this->_connections[$name];
        }
        if ( $this->_setups[$name] instanceof Closure )
        {
            $this->_connections[$name] = $this->_setups[$name]->__invoke();
        }

        return $this->_connections[$name];
    }


    public static function get(string $name) : AbstractPdo
    {
        return static::getInstance()->getConnection($name);
    }


    final public static function getInstance() : ConnectionPool
    {
        if ( ! static::$instance )
        {
            $calledClass        = get_called_class();
            static::$instance   = new $calledClass();
        }

        return static::$instance;
    }

    final private function __clone()
    {}
}