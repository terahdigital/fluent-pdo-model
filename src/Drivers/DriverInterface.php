<?php declare(strict_types=1);
/**
 * @package Terah\FluentPdoModel
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace Terah\FluentPdoModel\Drivers;

use Psr\Log\LoggerInterface;
use Terah\FluentPdoModel\Column;
use Terah\FluentPdoModel\ForeignKey;
use Terah\RedisCache\CacheInterface;

/**
 * Interface DriverInterface
 *
 * @package Terah\FluentPdoModel\Drivers
 * @author  Terry Cullen - terry@terah.com.au
 */
interface DriverInterface
{
    /**
     * @param array $config
     *
     * @return AbstractPdo
     */
    public function setConfig(array $config) : AbstractPdo;

    /**
     * @param $key
     * @return mixed
     */
    public function getConfig(string $key);

    /**
     * @return LoggerInterface
     */
    public function getLogger() : LoggerInterface;

    /**
     * @return CacheInterface
     */
    public function getCache() : CacheInterface;

    /**
     * @param bool $include_views
     * @param bool $flushTables
     * @return string[]
     */
    public function getTables(bool $include_views=false, bool $flushTables=false) : array;

    /**
     * @param bool $include_views
     * @param string $table
     * @param bool $flushTables
     * @return Column[]
     */
    public function getColumns(bool $include_views=false, string $table='', bool $flushTables=false) : array;

    /**
     * @param string $table
     * @param bool $flushTables
     * @return ForeignKey[][]
     */
    public function getForeignKeys(string $table='', bool $flushTables=false) : array;


    /**
     * @param bool|false $include_views
     * @param string $table
     * @param bool $flushTables
     * @return array
     */
    public function getTableCounts(bool $include_views=false, string $table='', bool $flushTables=false) : array;

    /**
     * @param string $table
     * @param string $column
     * @param bool $flushTables
     * @return string
     */
    public function getFieldComment(string $table, string $column, bool $flushTables=false) : string;
}