<?php declare(strict_types=1);
/**
 * @package Terah\FluentPdoModel
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace Terah\FluentPdoModel\Drivers;

use Exception;
use PDO;
use PDOException;
use PDOStatement;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Terah\FluentPdoModel\Column;
use Terah\FluentPdoModel\ForeignKey;
use Terah\RedisCache\CacheInterface;
use Terah\Assert\Assert;
use Terah\RedisCache\NullCache;

/**
 * PDOOCI
 *
 * PHP version 5.3
 *
 * @category OciStubPdo
 * @package  OciStubPdo
 * @author   Eustáquio Rangel <eustaquiorangel@gmail.com>
 * @license  http://www.gnu.org/licenses/gpl-2.0.html GPLv2
 * @link     http://github.com/taq/pdooci
 */

/**
 * Main class of OciStubPdo
 *
 * PHP version 5.3
 *
 * @category Connection
 * @package  OciStubPdo
 * @author   Eustáquio Rangel <eustaquiorangel@gmail.com>
 * @license  http://www.gnu.org/licenses/gpl-2.0.html GPLv2
 * @link     http://github.com/taq/pdooci
 */
class OciStubPdo extends AbstractPdo implements DriverInterface
{
    /**
     * @var resource
     */
    private $_con               = null;


    private bool $_autocommit   = true;

    /** @var array|int|string */
    private $_last_error  = [];


    private string $_charset    = '';

    /**
     * OciStubPdo constructor.
     * @param string $dsn
     * @param string $username
     * @param string $password
     * @param array $options
     * @param LoggerInterface|null $logger
     * @param CacheInterface|null $cache
     */
    public function __construct(string $dsn, string $username='', string $password='', array $options=[], LoggerInterface $logger=null, CacheInterface $cache=null)
    {
        if (!function_exists("\\oci_parse")) {
            throw new PDOException(PHP_VERSION . " : No support for Oracle, please install the OCI driver");
        }

        // find charset
        $charset                = null;
        $dsn                    = preg_replace('/^oci:/', '', $dsn);
        $tokens                 = preg_split('/;/', $dsn);
        $data                   = $tokens[0];
        $charset                = $this->_getCharset($tokens);

        try {
            if (!is_null($options) && array_key_exists(PDO::ATTR_PERSISTENT, $options)) {
                $this->_con = @oci_pconnect($username, $password, $data, $charset);
                $this->setError();
            } else {
                $this->_con = @oci_connect($username, $password, $data, $charset);
                $this->setError();
            }
            if (!$this->_con) {
                $error = oci_error();
                throw new Exception($error['code'] . ': ' . $error['message']);
            }
        } catch ( Exception $exception ) {
            throw new PDOException($exception->getMessage());
        }
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        $this->setConfig($options, 'oci:' . $data);
        $this->setLogger($logger ? $logger : new NullLogger());
        $this->setCache($cache ? $cache : new NullCache());
    }


    /**
     * @param bool $include_views
     * @param bool $flushTables
     * @return string[]
     */
    public function getTables(bool $include_views=false, bool $flushTables=false) : array
    {
        return [];
    }

    /**
     * @param bool $include_views
     * @param string $table
     * @param bool $flushTables
     * @return Column[][]
     */
    public function getColumns(bool $include_views=false, string $table='', bool $flushTables=false) : array
    {
        return [];
    }

    /**
     * @param string $table
     * @param bool $flushTables
     * @return ForeignKey[][]
     */
    public function getForeignKeys(string $table='', bool $flushTables=false) : array
    {
        return [];
    }

    /**
     * @param bool|false $include_views
     * @param string $table
     * @param bool $flushTables
     * @return array
     */
    public function getTableCounts(bool $include_views=false, string $table='', bool $flushTables=false) : array
    {
        return [];
    }


    public function getCharset() : string
    {
        return $this->_charset;
    }

    /**
     * Find the charset
     *
     * @param array $charset charset
     *
     * @return string charset
     */
    private function _getCharset(array $charset=[]) : string
    {
        if ( empty($charset) )
        {
            $langs = array_filter([getenv("NLS_LANG")], "strlen");

            return array_shift($langs);
        }

        $expr                   = '/^(charset=)(\w+)$/';
        $tokens                 = array_filter($charset, function ($token) use ($expr) {
            return preg_match($expr, $token, $matches);
        });
        $this->_charset         = '';
        if ( sizeof($tokens) > 0 )
        {
            preg_match($expr, array_shift($tokens), $matches);
            $this->_charset = $matches[2];
        }

        return $this->_charset;
    }

    /**
     * Return the connection
     *
     * @return resource handle
     */
    public function getConnection()
    {
        return $this->_con;
    }

    /**
     * Execute a query
     *
     * @param string $statement sql query
     * @param int|null $mode PDO query() mode
     * @param null $p1 PDO query() first parameter
     * @param null $p2 PDO query() second parameter
     *
     * @return OciPdoStubStatement
     */
    public function query(string $statement, ?int $mode=null, $p1=null, $p2=null) : PDOStatement
    {
        // TODO: use mode and parameters
        $stmt                   = null;
        try
        {
            $stmt                   = new OciPdoStubStatement($this, $statement);
            $stmt->execute();
            $this->setError();

            return $stmt;
        }
        catch ( Exception $e )
        {
            throw new PDOException($e->getMessage());
        }
    }


    public function exec(string $statement) : int
    {
        try
        {
            $stmt               = $this->query($statement);
            $rows               = $stmt->rowCount();
            $stmt->closeCursor();

            return $rows;
        }
        catch ( Exception $e )
        {
            throw new PDOException($e->getMessage());
        }
    }


    public function setAttribute(int $attr, $value) : bool
    {
        switch ( $attr )
        {
            case PDO::ATTR_AUTOCOMMIT:

                $this->_autocommit = ( is_bool($value) && $value ) || in_array(strtolower($value), ["on", "true"]);

        }

        return true;
    }

    /**
     * Return an attribute
     *
     * @param int $attr attribute
     *
     * @return mixed attribute value
     */
    public function getAttribute(int $attr)
    {
        switch ($attr) {
            case PDO::ATTR_AUTOCOMMIT:
                return $this->_autocommit;
            case PDO::ATTR_DRIVER_NAME:
                return 'oci';
        }

        return null;
    }

    /**
     * Return the auto commit flag
     *
     * @return boolean auto commit flag
     */
    public function getAutoCommit() : bool
    {
        return $this->_autocommit;
    }

    /**
     * Commit connection
     *
     * @return bool if commit was executed
     */
    public function commit() : bool
    {
        oci_commit($this->_con);
        $this->setError();

        return true;
    }

    /**
     * Rollback connection
     *
     * @return bool if rollback was executed
     */
    public function rollBack() : bool
    {
        oci_rollback($this->_con);
        $this->setError();

        return true;
    }


    public function beginTransaction() : bool
    {
        $this->setAttribute(PDO::ATTR_AUTOCOMMIT, false);

        return true;
    }


    public function setLimit(string $query, int $limit=0, int $offset=0) : string
    {
        Assert::that($query)->string()->notEmpty();
        Assert::that($limit)->nullOr()->integer();
        Assert::that($offset)->nullOr()->integer();
        if ( $offset )
        {
            $limit                  = $limit ?: 1;
            $limit                  = $limit + $offset;

            return "SELECT * FROM ({$query}) WHERE ROWNUM between {$offset} AND {$limit}";
        }
        if ( $limit  )
        {
            return "SELECT * FROM ({$query}) WHERE ROWNUM <= {$limit}";
        }

        return $query;
    }


    public function prepare(string $query, array $options=[]) : PDOStatement
    {
        try
        {
            return new OciPdoStubStatement($this, $query);
        }
        catch ( Exception $e )
        {
            throw new PDOException($e->getMessage());
        }
    }


    public function setError($obj=null) : void
    {
        $obj                    = $obj ? $obj : $this->_con;
        if ( ! is_resource($obj) )
        {
            return;
        }
        $error                  = oci_error($obj);
        if ( ! $error )
        {
            return ;
        }
        $this->_last_error = $error;
    }


    public function errorCode() : string
    {
        if ( ! $this->_last_error )
        {
            return '';
        }

        return strval($this->_last_error["code"]);
    }


    public function errorInfo() : array
    {
        if ( ! $this->_last_error )
        {
            return [];
        }

        return [
            $this->_last_error["code"],
            $this->_last_error["code"],
            $this->_last_error["message"]
        ];
    }

    /**
     * Close connection
     *
     * @return null
     */
    public function close()
    {
        if (is_null($this->_con)) {
            return;
        }
        oci_close($this->_con);
        $this->_con = null;

        return null;
    }

    /**
     * Trigger stupid errors who should be exceptions
     *
     * @param int $errno error number
     * @param string $errstr error message
     * @param mixed $errfile error file
     * @param int|null $errline error line
     * @param array|null $errcontext
     * @return bool
     */
    public function errorHandler( int $errno , string $errstr , ?string $errfile=null , ?int $errline=null , ?array $errcontext=null) : bool
    {
        unset($errno, $errfile, $errline);
        preg_match('/(ORA-)(\d+)/', $errstr, $ora_error);
        if ( $ora_error )
        {
            $this->_last_error = intval($ora_error[2]);

            return true;
        }
        $this->setError($this->_con);

        return true;
    }

    /**
     * Return available drivers
     * Will insert the OCI driver on the list, if not exist
     *
     * @return array with drivers
     */
    public static function getAvailableDrivers() : array
    {
        $drivers                = PDO::getAvailableDrivers();
        if ( ! in_array("oci", $drivers) )
        {
            array_push($drivers, "oci");
        }

        return $drivers;
    }

    /**
     * Return if is on a transaction
     *
     * @return boolean on a transaction
     */
    public function inTransaction() : bool
    {
        return ! $this->_autocommit;
    }


    public function quote(string $string, ?int $type = null) : string
    {
        $string                 = preg_replace('/\'/', "''", $string);
        $string                 = "'$string'";

        return $string;
    }

    /**
     * Return the last inserted id
     * If the sequence name is not sent, throws an exception
     *
     * @param string $sequence name
     *
     * @return integer last id
     * @throws PDOException
     */
    public function lastInsertId($sequence = null) : int
    {
        if (!$sequence) {
            throw new PDOException("SQLSTATE[IM001]: Driver does not support this function: driver does not support getting attributes in system_requirements");
        }
        $id = 0;
        try {
            $stmt = $this->query("select $sequence.currval from dual");
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            $id   = intval($data["CURRVAL"]);
        } catch ( PDOException $e ) {
        }
        return $id;
    }

    /**
     * @param string $table
     * @param string $column
     * @param bool $flushTables
     * @return string
     */
    public function getFieldComment(string $table, string $column, bool $flushTables=false) : string
    {
        return '';
    }
}
