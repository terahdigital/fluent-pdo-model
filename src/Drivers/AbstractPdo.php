<?php declare(strict_types=1);
/**
 * @package Terah\FluentPdoModel
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace Terah\FluentPdoModel\Drivers;

use Psr\Log\LoggerInterface;
use \PDO;
use Terah\Assert\Assert;
use Psr\Log\LoggerAwareTrait;
use Terah\FluentPdoModel\Column;
use Terah\FluentPdoModel\ForeignKey;
use Terah\RedisCache\RedisCacheTrait;
/**
 * Class AbstractPdo
 *
 * @package Terah\FluentPdoModel\Drivers
 * @author  Terry Cullen - terry@terah.com.au
 */
abstract class AbstractPdo extends PDO implements DriverInterface
{
    use RedisCacheTrait;
    use LoggerAwareTrait;

    protected array $_config            = [];


    protected int $_transactionCount    = 0;


    protected bool $_supportsColumnMeta = false;


    public function getLogger() : LoggerInterface
    {
        return $this->logger;
    }


    public function setConfig(array $config=[], string $dsn='') : AbstractPdo
    {
        if ( $dsn )
        {
            $driver                 = strpos($dsn, ':') !== false ? substr($dsn, 0, strpos($dsn, ':')) : $dsn;
            $driver                 = ucfirst(strtolower($driver));
            $dsn                    = preg_replace("/^{$driver}:/i", '', $dsn);
            $parts                  = explode(';', $dsn);
            foreach ( $parts as $conf )
            {
                $conf                   = explode('=', $conf);
                $config[$conf[0]]       = isset($conf[1]) ? $conf[1] : true;
            }
            $config['driver']       = $driver;
        }
        $this->_config          = $config;

        return $this;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getConfig(string $key)
    {
        return isset($this->_config[$key]) ? $this->_config[$key] : null;
    }

    /**
     * @return array
     */
    public function getAllConfig() : array
    {
        return $this->_config;
    }

    /**
     * @return bool
     */
    public function logQueries() : bool
    {
        return !empty( $this->_config['log_queries'] ) && $this->_config['log_queries'];
    }

    /**
     * @return int
     */
    public function getTransactionDepth() : int
    {
        return $this->_transactionCount;
    }

    /**
     * @return bool
     */
    public function beginTransaction() : bool
    {
        $this->_transactionCount++;
        if ( $this->_transactionCount === 1 )
        {
            return parent::beginTransaction();
        }

        return true;
    }

    /**
     * @return bool
     */
    public function commit() : bool
    {
        $this->_transactionCount--;
        $this->_transactionCount = $this->_transactionCount < 0 ? 0 : $this->_transactionCount;
        if ( $this->_transactionCount === 0 )
        {
            return parent::commit();
        }

        return true;
    }

    /**
     * @return bool
     */
    public function rollback() : bool
    {
        if ( $this->_transactionCount > 0 )
        {
            $this->_transactionCount = 0;

            return parent::rollBack();
        }

        return true;
    }

    /**
     * @param string $query
     * @param integer $limit
     * @param integer $offset
     * @return string
     */
    public function setLimit(string $query, int $limit=0, int $offset=0) : string
    {
        Assert::that($query)->string()->notEmpty();
        Assert::that($limit)->unsignedInt();
        Assert::that($offset)->unsignedInt();
        if ( $limit )
        {
            $query .= " LIMIT {$limit}";
        }
        if ( $offset )
        {
            $query .= " OFFSET {$offset}";
        }

        return $query;
    }

    /**
     * @return bool
     */
    public function supportsColumnMeta() : bool
    {
        return $this->_supportsColumnMeta;
    }

    /**
     * @param string $table
     * @param string $column
     * @param bool $flushTables
     * @return string
     */
    abstract public function getFieldComment(string $table, string $column, bool $flushTables=false) : string;

    /**
     * @param bool $include_views
     * @param bool $flushTables
     * @return string[]
     */
    abstract public function getTables(bool $include_views=false, bool $flushTables=false) : array;

    /**
     * @param bool $include_views
     * @param string $table
     * @param bool $flushTables
     * @return Column[][]
     */
    abstract public function getColumns(bool $include_views=false, string $table='', bool $flushTables=false) : array;

    /**
     * @param string $table
     * @param bool $flushTables
     * @return ForeignKey[][]
     */
    abstract public function getForeignKeys(string $table='', bool $flushTables=false) : array;


    /**
     * @param bool|false $include_views
     * @param string $table
     * @param bool $flushTables
     * @return array
     */
    abstract public function getTableCounts(bool $include_views=false, string $table='', bool $flushTables=false) : array;
}