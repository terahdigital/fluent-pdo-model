<?php declare(strict_types=1);
/**
 * PDOOCI
 *
 * PHP version 5.3
 *
 * @category PDOOCI
 * @package  PDOOCI
 * @author   Eustáquio Rangel <eustaquiorangel@gmail.com>
 * @license  http://www.gnu.org/licenses/gpl-2.0.html GPLv2
 * @link     http://github.com/taq/pdooci
 */

namespace Terah\FluentPdoModel\Drivers;

use Exception;
use PDO;
use PDOException;
use PDOStatement;
use ReflectionFunction;
use stdClass;

/**
 * Statement class of PDOOCI
 *
 * PHP version 5.3
 *
 * @category FluentPdoModel
 * @package  PDOOCI
 * @author   Eustáquio Rangel <eustaquiorangel@gmail.com>
 * @license  http://www.gnu.org/licenses/gpl-2.0.html GPLv2
 * @link     http://github.com/taq/pdooci
 */
class OciPdoStubStatement extends PDOStatement
{

    private ?PDO $_pdooci = null;

    /** @var resource */
    private $_con = null;

    private string $_statement = '';

    /** @var resource */
    private $_stmt = null;


    private ?int $_fetch_sty = null;

    /** @var mixed */
    private $_current = null;


    private int $_pos = 0;


    private array $_binds = [];


    protected string $_queryString = '';

    /**
     * Constructor
     *
     * @param OciStubPdo    $pdooci    PDOOCI connection
     * @param string $statement sql statement
     *
     * @throws PDOException
     */
    public function __construct(OciStubPdo $pdooci, string $statement)
    {
        try
        {
            $this->_pdooci    = $pdooci;
            $this->_con       = $pdooci->getConnection();
            $this->_statement = OciPdoStubStatement::insertMarks($statement);
            $this->_stmt      = oci_parse($this->_con, $this->_statement);
            $this->_fetch_sty = PDO::FETCH_BOTH;

            $this->_queryString = $this->_statement;
        }
        catch ( Exception $e )
        {
            throw new PDOException($e->getMessage());
        }
    }

    /**
     * Binds a value
     *
     * @param mixed $param param (column)
     * @param mixed $value value for param
     * @param mixed $type  optional data type
     *
     * @return bool bound
     * @throws PDOException
     */
    public function bindValue($param, $value, int $type = PDO::PARAM_STR) : bool
    {
        try
        {
            $param                = $this->_getBindVar($param);
            $ok                   = oci_bind_by_name($this->_stmt, $param, $value);
            $this->_binds[$param] = $value;
        }
        catch ( Exception $e )
        {
            throw new PDOException($e->getMessage());
        }

        return $ok;
    }


    public function bindParam($parameter, &$variable, int $data_type=PDO::PARAM_STR, int $length = null, $driver_options=null) : bool
    {
        try
        {
            $parameter              = $this->_getBindVar($parameter);
            $ok                     = oci_bind_by_name($this->_stmt, $parameter, $variable);
            $this->_binds[$parameter] = $variable;
        }
        catch ( Exception $e )
        {
            throw new PDOException($e->getMessage());
        }

        return $ok;
    }

    /**
     * Get the variable name for binding
     *
     * @param mixed $val variable value
     *
     * @return string correct name for binding
     */
    private function _getBindVar($val) : string
    {
        if ( preg_match('/^\d+$/', (string)$val) )
        {
            $val = ":pdooci_m" . ( intval($val) - 1 );

            return $val;
        }
        else if ( preg_match('/^:/', (string)$val) )
        {
            return $val;
        }
        return ":{$val}";
    }

    /**
     * Execute statement
     *
     * @param mixed $values optional values
     *
     * @return boolean
     * @throws PDOException
     */
    public function execute(?array $values = null) : bool
    {
        set_error_handler([$this->_pdooci, "errorHandler"]);
        try
        {
            $auto                   = $this->_pdooci->getAutoCommit() ? OCI_COMMIT_ON_SUCCESS : OCI_NO_AUTO_COMMIT;

            if ( $values && sizeof($values) > 0 )
            {
                foreach ( $values as $key => $val )
                {
                    $parm               = $key;
                    if ( preg_match('/^\d+$/', (string)$key) )
                    {
                        $parm++;
                    }
                    $this->bindValue($parm, $values[$key]);
                    $this->_pdooci->setError();
                }
            }
            $ok                     = oci_execute($this->_stmt, $auto);
            if ( ! $ok )
            {
                $this->_pdooci->setError($this->_stmt);
                $error                  = $this->_pdooci->errorInfo();

                throw new PDOException($error[2]);
            }
        }
        catch ( Exception $e )
        {
            throw new PDOException($e->getMessage());
        }
        restore_error_handler();

        return true;
    }

    /**
     * Get the number of affected rows
     *
     * @return int number of rows
     * @throws PDOException
     */
    public function rowCount() : int
    {
        set_error_handler([$this->_pdooci, "errorHandler"]);
        $rows = null;
        try
        {
            $rows = oci_num_rows($this->_stmt);
        }
        catch ( Exception $e )
        {
            throw new PDOException($e->getMessage());
        }
        restore_error_handler();

        return $rows;
    }


    public function closeCursor() : bool
    {
        set_error_handler([$this->_pdooci, "errorHandler"]);
        try
        {
            oci_free_statement($this->_stmt);
        }
        catch ( Exception $e )
        {
            throw new PDOException($e->getMessage());
        }
        restore_error_handler();

        $this->_stmt = null;

        return true;
    }

    /**
     * Fetch a value
     *
     * @param null $how
     * @param null $orientation
     * @param null $offset
     *
     * @return array|mixed|null|object
     */
    public function fetch($how = null, $orientation = null, $offset = null)
    {
        set_error_handler([$this->_pdooci, "errorHandler"]);
        try
        {
            $style                  = !$how ? $this->_fetch_sty : $how;
            $this->_fetch_sty       = $style;
            $rst                    = null;

            switch ( $style )
            {
                case PDO::FETCH_BOTH:
                case PDO::FETCH_BOUND:
                    $rst = oci_fetch_array($this->_stmt, OCI_BOTH + OCI_RETURN_NULLS);
                    break;
                case PDO::FETCH_ASSOC:
                    $rst = oci_fetch_array($this->_stmt, OCI_ASSOC + OCI_RETURN_NULLS);
                    break;
                case PDO::FETCH_NUM:
                    $rst = oci_fetch_array($this->_stmt, OCI_NUM + OCI_RETURN_NULLS);
                    break;
                case PDO::FETCH_OBJ:
                    $rst = oci_fetch_object($this->_stmt);
                    break;
            }
            $this->_current         = $rst;
            $this->_checkBinds();
        }
        catch ( Exception $e )
        {
            throw new PDOException($e->getMessage());
        }
        restore_error_handler();

        return $rst;
    }


    public function fetchAll( int $fetch_style=PDO::FETCH_BOTH, $fetch_argument=null, array $ctor_args=[] ) : array
    {
        // $style                  = is_null($fetch_style) ? PDO::FETCH_BOTH : $how;
        $rst   = null;
        try
        {
            switch ($fetch_style)
            {
                case PDO::FETCH_ASSOC:
                    oci_fetch_all($this->_stmt, $rst, 0, -1, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC);
                    break;

                case PDO::FETCH_BOTH:
                    oci_fetch_all($this->_stmt, $rst, 0, -1, OCI_FETCHSTATEMENT_BY_ROW + OCI_NUM + OCI_ASSOC);
                    break;

                case PDO::FETCH_COLUMN:
                    oci_fetch_all($this->_stmt, $rst, 0, -1, OCI_FETCHSTATEMENT_BY_ROW + OCI_NUM);
                    $rst = array_map(function ($vals) use ($fetch_argument) {
                        return $vals[intval($fetch_argument)];
                    }, $rst);
                    break;

                case PDO::FETCH_COLUMN | PDO::FETCH_GROUP:
                    oci_fetch_all($this->_stmt, $rst, 0, -1, OCI_FETCHSTATEMENT_BY_ROW + OCI_NUM);
                    $temp = [];
                    foreach ($rst as $value) {
                        if (!array_key_exists($value[0], $temp)) {
                            $temp[$value[0]] = [];
                        }
                        array_push($temp[$value[0]], $value[1]);
                    }
                    $rst = $temp;
                    break;

                case PDO::FETCH_CLASS:
                    oci_fetch_all($this->_stmt, $rst, 0, -1, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC);
                    $temp = [];
                    foreach ($rst as $data) {
                        array_push($temp, $this->_createObjectFromData((string)$fetch_argument, $data));
                    }
                    $rst = $temp;
                    break;

                case PDO::FETCH_FUNC:
                    if (!function_exists($fetch_argument)) {
                        throw new PDOException("Function $fetch_argument does not exists");
                    }
                    $ref  = new ReflectionFunction($fetch_argument);
                    $args = $ref->getNumberOfParameters();
                    if ($args < 1) {
                        throw new PDOException("Function $fetch_argument can't receive parameters");
                    }
                    oci_fetch_all($this->_stmt, $rst, 0, -1, OCI_FETCHSTATEMENT_BY_ROW + OCI_NUM);
                    foreach ($rst as $value) {
                        $temp = [];
                        foreach ($value as $key => $data) {
                            array_push($temp, $data);
                        }
                        call_user_func_array($fetch_argument, $temp);
                    }
                    break;
            }
        }
        catch ( Exception $e )
        {
            throw new PDOException($e->getMessage());
        }

        return $rst;
    }

    /**
     * Fetch column
     *
     * @param int $colnum optional column number
     *
     * @return mixed column value
     */
    public function fetchColumn($colnum = 0)
    {
        $rst = $this->fetch(PDO::FETCH_NUM);

        return $rst[$colnum];
    }


    public function fetchObject($class_name='stdClass', $ctor_args=null) : ?object
    {
        try
        {
            $data = $this->fetch(PDO::FETCH_ASSOC);

            return $this->_createObjectFromData($class_name, $data);
        }
        catch ( Exception $e )
        {
            return null;
        }
    }


    private function _createObjectFromData(string $name, $data) : ?object
    {
        try
        {
            $cls                    = new $name();
            foreach ( $data as $key => $value )
            {
                if ( $name !== 'stdClass' && !array_key_exists(strtolower($key), get_object_vars($cls)) )
                {
                    continue;
                }
                $key                    = strtolower($key);
                $cls->$key              = $value;
            }
            return $cls;
        }
        catch ( Exception $e )
        {
            return null;
        }
    }

    /**
     * Convert a query to use bind marks
     *
     * @param string $query to insert bind marks
     *
     * @return string query with bind marks
     */
    public static function insertMarks(string $query) : string
    {
        preg_match_all('/\?/', $query, $marks);
        if ( sizeof($marks[0]) < 1 )
        {
            return $query;
        }
        foreach ( $marks[0] as $idx => $mark )
        {
            $query                  = preg_replace("/\?/", ":pdooci_m$idx", $query, 1);
        }

        return $query;
    }

    /**
     * Return the current statement
     *
     * @return string statement
     */
    public function getStatement() : string
    {
        return $this->_statement;
    }

    /**
     * Return the current value
     *
     * @return null
     */
    public function current()
    {
        if (!$this->_current) {
            $this->next();
            if (!$this->_current) {
                $this->_pos = -1;
                $this->closeCursor();
            }
        }
        return $this->_current;
    }

    /**
     * Return the current key/position
     *
     * @return integer
     */
    public function key()
    {
        return $this->_pos;
    }

    /**
     * Return the next value
     */
    public function next() : void
    {
        $this->_current = $this->fetch(PDO::FETCH_ASSOC);
        if (!$this->_current) {
            $this->_pos = -1;
        }
        $this->_checkBinds();
    }


    public function rewind() : void
    {
        $this->_pos = 0;
    }

    public function valid() : bool
    {
        return $this->_pos >= 0;
    }


    public function setFetchMode(int $mode, string $className='', array $params=[]) : bool
    {
        unset($p1, $p2);

        $this->_fetch_sty       = $mode;

        return true;
    }



    public function getFetchMode() : int
    {
        return (int)$this->_fetch_sty;
    }


    public function bindColumn($column, &$param, int $type=null, int $maxlen = null, $driver=null) : bool
    {
        $column                 = is_numeric($column) ? $column : strtoupper($column);

        $this->_binds[$column]  = &$param;

        return true;
    }


    private function _checkBinds() : void
    {
        if ( $this->_fetch_sty != PDO::FETCH_BOUND )
        {
            return;
        }
        foreach ( $this->_binds as $key => &$value )
        {
            if (is_numeric($key)) {
                $key--;
            } else {
                $key = strtoupper($key);
            }
            if (!array_key_exists($key, $this->_current)) {
                continue;
            }
            $value = $this->_current[$key];
        }
    }


    public function columnCount() : int
    {
        if ( ! $this->_stmt )
        {
            return 0;
        }
        try
        {
            return (int)oci_num_fields($this->_stmt);
        }
        catch ( Exception $e )
        {
            throw new PDOException($e->getMessage());
        }
    }


    public function debugDumpParams() : void
    {
        $str                    = "SQL: [" . strlen($this->_statement) . "] " . $this->_statement . "\n";
        $str                    .= "Params: " . sizeof($this->_binds) . "\n";
        foreach ($this->_binds as $key => $value)
        {
            $str                    .= "Key: Name: [" . strlen($key) . "] $key\n";
            $str                    .= "name=[" . strlen($key) . "] \"$key\"\n";
            $str                    .= "is_param=1\n";
        }

        echo substr($str, 0, strlen($str) - 1);
    }

    /**
     * Return error code
     *
     * @return mixed error code
     */
    public function errorCode()
    {
        return $this->_pdooci->errorCode();
    }

    /**
     * Return error info
     *
     * @return mixed error info
     */
    public function errorInfo()
    {
        return $this->_pdooci->errorInfo();
    }


    /**
     * @param int $attribute
     * @param mixed $value
     * @return bool
     */
    public function setAttribute(int $attribute, $value) : bool
    {
        // nothing to see here
        return true;
    }



    /**
     * @param int $attribute
     * @return null
     */
    public function getAttribute(int $attribute)
    {
        // nothing to see here
        return null;
    }


    public function getColumnMeta(int $colnum=0) : array
    {
        if ( ! $this->_stmt )
        {
            return [];
        }
        $name                   = oci_field_name($this->_stmt, $colnum + 1);
        $len                    = oci_field_size($this->_stmt, $colnum + 1);
        $prec                   = oci_field_scale($this->_stmt, $colnum + 1);
        $type                   = oci_field_type($this->_stmt, $colnum + 1);

        return [
            "name"                  => $name,
            "len"                   => $len,
            "precision"             => $prec,
            "driver:decl_type"      => $type
        ];
    }


    public function nextRowSet() : bool
    {
        return true;
    }
}
