<?php declare(strict_types=1);
/**
 * @package Terah\FluentPdoModel
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace Terah\FluentPdoModel\Drivers;

use \PDO;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Terah\FluentPdoModel\Column;
use Terah\FluentPdoModel\ForeignKey;
use Terah\RedisCache\CacheInterface;
use Terah\RedisCache\NullCache;
use Terah\Assert\Assert;

class OciPdo extends AbstractPdo implements DriverInterface
{
    /**
     * @param string $dsn
     * @param string $username
     * @param string $password
     * @param array $options
     * @param LoggerInterface|null $logger
     * @param CacheInterface|null $cache
     */
    public function __construct(string $dsn, string $username='', string $password='', array $options=[], LoggerInterface $logger=null, CacheInterface $cache=null)
    {
        parent::__construct($dsn, $username, $password, $options);
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        $this->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
        if ( array_key_exists('timeout', $options) && $options['timeout'] )
        {
            $this->setAttribute(PDO::ATTR_TIMEOUT, (int)$options['timeout']);
        }
        $this->setConfig($options, $dsn);
        $this->setLogger($logger ? $logger : new NullLogger());
        $this->setCache($cache ? $cache : new NullCache());
    }

    /**
     * @param bool $include_views
     * @param bool $flushTables
     * @return string[]
     */
    public function getTables(bool $include_views=false, bool $flushTables=false) : array
    {
        return [];
    }

    /**
     * @param bool $include_views
     * @param string $table
     * @param bool $flushTables
     * @return Column[][]
     */
    public function getColumns(bool $include_views=false, string $table='', bool $flushTables=false) : array
    {
        return [];
    }

    /**
     * @param string $table
     * @param bool $flushTables
     * @return ForeignKey[][]
     */
    public function getForeignKeys(string $table='', bool $flushTables=false) : array
    {
        return [];
    }

    /**
     * @param bool|false $include_views
     * @param string $table
     * @param bool $flushTables
     * @return array
     */
    public function getTableCounts(bool $include_views=false, string $table='', bool $flushTables=false) : array
    {
        return [];
    }

    /**
     * @param string $table
     * @param string $column
     * @param bool $flushTables
     * @return string
     */
    public function getFieldComment(string $table, string $column, bool $flushTables=false) : string
    {
        return '';
    }

    /**
     * @param string $query
     * @param integer $limit
     * @param null|integer $offset
     * @return string
     */
    public function setLimit(string $query, int $limit=0, int $offset=0) : string
    {
        Assert::that($query)->string()->notEmpty();
        Assert::that($limit)->nullOr()->integer();
        Assert::that($offset)->nullOr()->integer();
        if ( $offset )
        {
            $limit  = $limit ?: 1;
            $limit  = $limit + $offset;
            return "SELECT * FROM ({$query}) WHERE ROWNUM between {$offset} AND {$limit}";
        }
        if ( $limit  )
        {
            return "SELECT * FROM ({$query}) WHERE ROWNUM <= {$limit}";
        }

        return $query;
    }
}
