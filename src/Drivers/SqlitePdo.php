<?php declare(strict_types=1);
/**
 * @package Terah\FluentPdoModel
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace Terah\FluentPdoModel\Drivers;

use \PDO;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Terah\FluentPdoModel\Column;
use Terah\FluentPdoModel\ForeignKey;
use Terah\RedisCache\CacheInterface;
use Terah\RedisCache\NullCache;

/**
 * Class SqlitePdo
 *
 * @package Terah\FluentPdoModel\Drivers
 * @author  Terry Cullen - terry@terah.com.au
 */
class SqlitePdo extends AbstractPdo implements DriverInterface
{
    protected bool $_supportsColumnMeta = true;

    /**
     * SqlitePdo constructor.
     * @param string $dsn
     * @param string $username
     * @param string $password
     * @param array $options
     * @param LoggerInterface|null $logger
     * @param CacheInterface|null $cache
     */
    public function __construct(string $dsn, string $username='', string $password='', array $options=[], LoggerInterface $logger=null, CacheInterface $cache=null)
    {
        parent::__construct($dsn, $username, $password, $options);
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        $this->setConfig($options, $dsn);
        $this->setLogger($logger ? $logger : new NullLogger());
        $this->setCache($cache ? $cache : new NullCache());
    }


    /**
     * @param bool $include_views
     * @param bool $flushTables
     * @return string[]
     */
    public function getTables(bool $include_views=false, bool $flushTables=false) : array
    {
        return [];
    }

    /**
     * @param bool $include_views
     * @param string $table
     * @param bool $flushTables
     * @return Column[][]
     */
    public function getColumns(bool $include_views=false, string $table='', bool $flushTables=false) : array
    {
        return [];
    }

    /**
     * @param string $table
     * @param bool $flushTables
     * @return ForeignKey[][]
     */
    public function getForeignKeys(string $table='', bool $flushTables=false) : array
    {
        return [];
    }

    /**
     * @param bool|false $include_views
     * @param string $table
     * @param bool $flushTables
     * @return array
     */
    public function getTableCounts(bool $include_views=false, string $table='', bool $flushTables=false) : array
    {
        return [];
    }

    /**
     * @param string $table
     * @param string $column
     * @param bool $flushTables
     * @return string
     */
    public function getFieldComment(string $table, string $column, bool $flushTables=false) : string
    {
        return '';
    }
}