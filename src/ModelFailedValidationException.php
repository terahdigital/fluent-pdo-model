<?php declare(strict_types=1);

namespace Terah\FluentPdoModel;

use Exception;

class ModelFailedValidationException extends Exception
{
    private array $validationErrors = [];


    public function getValidationErrors() : array
    {
        return is_array($this->validationErrors) ? $this->validationErrors : [];
    }

    /**
     * @param string         $message
     * @param array          $validationErrors
     * @param int            $code
     * @param Exception|null $previous
     */
    public function __construct($message="", array $validationErrors=[], $code=0, Exception $previous = null)
    {
        $this->validationErrors = $validationErrors;
        parent::__construct($message, $code, $previous);
    }
}