<?php declare(strict_types=1);
/**
  * @package Terah\FluentPdoModel
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @author        Terry Cullen - terry@terah.com.au
 */

namespace Terah\FluentPdoModel;

use Psr\Log\AbstractLogger;

class StdOutLogger extends AbstractLogger
{
    /**
     * Logs with an arbitrary level.
     *
     * @param mixed   $level
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     *
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function log($level, $message, array $context=[])
    {
        echo "[{$level}] - {$message}" . PHP_EOL;

        return null;
    }
}
