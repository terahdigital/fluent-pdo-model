<?php declare(strict_types=1);

namespace Terah\FluentPdoModel;

class ForeignKey
{
    public string $constraintName       = '';

    public string $localTableName       = '';

    public string $localColumnName      = '';

    public string $foreignTableName     = '';

    public string $foreignColumnName    = '';
}