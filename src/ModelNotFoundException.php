<?php declare(strict_types=1);

namespace Terah\FluentPdoModel;

use Exception;

class ModelNotFoundException extends Exception
{

}