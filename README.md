# fluent-pdo-model

[![Latest Version](https://img.shields.io/github/release/terah/fluent-pdo-model.svg?style=flat-square)](https://bitbucket.org/terahdigital/fluent-pdo-model/releases)
[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.md)
[![Build Status](https://img.shields.io/travis/terah/fluent-pdo-model/master.svg?style=flat-square)](https://travis-ci.org/terah/fluent-pdo-model)
[![Coverage Status](https://img.shields.io/scrutinizer/coverage/g/terah/fluent-pdo-model.svg?style=flat-square)](https://scrutinizer-ci.com/g/terah/fluent-pdo-model/code-structure)
[![Quality Score](https://img.shields.io/scrutinizer/g/terah/fluent-pdo-model.svg?style=flat-square)](https://scrutinizer-ci.com/g/terah/fluent-pdo-model)
[![Total Downloads](https://img.shields.io/packagist/dt/league/fluent-pdo-model.svg?style=flat-square)](https://packagist.org/packages/league/fluent-pdo-model)

FluentPdoModel: A super light orm with a fluent query builder, validations, caching, logging, and events.

This library provides;

1. A fluent query builder with pagination, caching, logging, validations ~~and events~~(events have been removed until hhvm has fixed some memory leaks)
2. Database models with validations.
3. A static class interface for ease of access of your models.

## Install

Via Composer

``` bash
$ composer require terah/fluent-pdo-model
```

## Usage

### Please see [Examples](bin/examples.php) for details.

## Testing

``` bash
$ phpunit
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security

If you discover any security related issues, please email terry@terah.com.au instead of using the issue tracker.

## Credits

- [Terry Cullen](https://bitbucket.org/terahdigital)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
