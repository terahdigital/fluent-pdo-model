<?php declare(strict_types=1);
/**
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @author        Terry Cullen - terry@terah.com.au
 */

/*
This library provides a few different ways to fetch, update, insert and delete your database data.

At the heart of this library is the FluentPdoModel.  The fluent query builder allows you to query the database
using a fluent interface (also known as chaining).

First of all, before we can use the FluentPdoModel, we need to create a connection:
 */

// Load the composer autoloader
require __DIR__ . '/../../../autoload.php';

use Psr\Log\NullLogger;
use Terah\FluentPdoModel\FluentPdoModel;
use Terah\FluentPdoModel\Drivers\SqlitePdo;
use Terah\FluentPdoModel\StdOutLogger;

//Create a new SqlitePdo connection (in memory for testing)
$connection     = new SqlitePdo('sqlite:memory:', '', '', ['log_queries' => true], new NullLogger());
// Load the test data
$connection->exec(file_get_contents(__DIR__ . '/../tests/test_data.sql'));
// Create a new query builder
$fluentQuery    = new FluentPdoModel($connection);

// +++++++++++++++++++++++++++++++++++
//            FETCHING DATA
// +++++++++++++++++++++++++++++++++++

// SELECT * FROM users
print_r(
    $fluentQuery->table('users')->fetch()
);
// SAME As $fluentQuery->table('users')->select('*')->fetch()
/*
returns an array of stdClass:
Array
(
    [0] => stdClass Object
        (
            [id] => 1
            [username] => admin
            [name] => Fred Flintstone
            [email] => fred@bedrock.com.rock
            [comments] => The Admin
            [status] => 1
        )
)
 */

// SELECT * FROM users WHERE id = 1 LIMIT 1
print_r(
    $fluentQuery->table('users')->fetchOne(1)
);
/*
stdClass Object
(
    [id] => 1
    [username] => admin
    [name] => Fred Flintstone
    [email] => fred@bedrock.com.rock
    [comments] => The Admin
    [status] => 1
)
 */

//  SELECT username FROM users LIMIT 1
print_r(
    $fluentQuery->table('users')->select('username')->fetchOne()
);
/*
stdClass Object
(
    [username] => admin
)
 */

// SELECT username, email FROM users LIMIT 1
print_r(
    $fluentQuery->table('users')->select(['username', 'email'])->fetchOne()
);
/*
stdClass Object
(
    [username] => admin
    [email] => fred@bedrock.com.rock
)
 */

// FIND FIELD

// SELECT username FROM users WHERE id = 1 LIMIT 1
print_r(
    $fluentQuery->table('users')->fetchStr('email', 1)
);
// fred@bedrock.com.rock

// TABLE ALIAS

// SELECT TheUsers.email FROM users AS TheUsers WHERE TheUsers.id = 1 LIMIT 1
print_r(
    $fluentQuery->table('users', 'TheUsers')->fetchStr('email', 1)
);
// fred@bedrock.com.rock

// COLUMN ALIAS

//  SELECT TheUsers.email AS myEmailAddress FROM users AS TheUsers WHERE TheUsers.id = 1 LIMIT 1
$fluentQuery->table('users', 'TheUsers')->fetchStr('email AS myEmailAddress', 1);


// WHERE CLAUSES

// SELECT * FROM users WHERE username = 'admin' LIMIT 1
$fluentQuery->table('users')->where('username', 'admin')->fetchOne();

// SELECT * FROM users WHERE username = 'admin' AND status = 1 LIMIT 1
$fluentQuery->table('users')->whereArr(['username' => 'admin', 'status' => 1])->fetchOne();

// SELECT * FROM users WHERE (username IN ('admin')) LIMIT 1
$fluentQuery->table('users')->whereIn('username', ['admin'])->fetchOne();

// SELECT * FROM users WHERE id = 1 LIMIT 1
$fluentQuery->table('users')->wherePk(1)->fetchOne();

// SELECT * FROM users WHERE username != 'admin' LIMIT 1
$fluentQuery->table('users')->whereNot('username', 'admin')->fetchOne();

// SELECT * FROM users WHERE email LIKE 'fred%' LIMIT 1
$fluentQuery->table('users')->whereLike('email', 'fred%')->fetchOne();

// SELECT * FROM users WHERE email NOT LIKE 'fred%' LIMIT 1
$fluentQuery->table('users')->whereNotLike('email', 'fred%')->fetchOne();

// SELECT * FROM movies WHERE rating > 8 LIMIT 1
$fluentQuery->table('movies')->whereGt('rating', 8)->fetchOne();

// SELECT * FROM movies WHERE rating < 8 LIMIT 1
$fluentQuery->table('movies')->whereLt('rating', 8)->fetchOne();

// SELECT * FROM movies WHERE rating >= 8 LIMIT 1
$fluentQuery->table('movies')->whereGte('rating', 8)->fetchOne();

// SELECT * FROM movies WHERE rating <= 8 LIMIT 1
$fluentQuery->table('movies')->whereLte('rating', 8)->fetchOne();

//  SELECT * FROM movies WHERE (rating IN (8.1, 8.6, 9))
$fluentQuery->table('movies')->whereIn('rating', [8.1, 8.6, 9])->fetch();

//  SELECT * FROM movies WHERE (rating NOT IN (8.1, 8.6, 9))
$fluentQuery->table('movies')->whereNotIn('rating', [8.1, 8.6, 9])->fetch();

//  SELECT * FROM movies WHERE (rating IS NOT NULL)
$fluentQuery->table('movies')->whereNull('rating')->fetch();

//  SELECT * FROM movies WHERE (rating IS NOT NULL)
$fluentQuery->table('movies')->whereNotNull('rating')->fetch();

//  SELECT name FROM movies WHERE rating > 9 OR rating < 8 LIMIT 1
$fluentQuery->table('movies')
    ->whereGt('rating', 9)
    ->_or()
    ->whereLt('rating', 8)
    ->fetchField('name');

// SELECT name FROM movies WHERE (rating > 9 OR rating < 8) AND name LIKE 'The%' LIMIT 1
$fluentQuery->table('movies')
    ->whereGt('rating', 9)
    ->_or()
    ->whereLt('rating', 8)
    ->wrap()
    ->_and()
    ->whereLike('name', 'The%')
    ->fetchField('name');

//  SELECT name FROM movies WHERE (rating > 9 OR rating < 8) AND (name LIKE 'The%' OR name NOT LIKE 'The road%') LIMIT 1
$fluentQuery->table('movies')
    ->whereGt('rating', 9)
    ->_or()
    ->whereLt('rating', 8)
    ->wrap()
    ->_and()
    ->whereLike('name', 'The%')
    ->_or()
    ->whereNotLike('name', 'The road%')
    ->wrap()
    ->fetchField('name');

// RAW SQL QUERIES
print_r(
    $fluentQuery->query('SELECT name, rating FROM movies WHERE name LIKE :name', ['name' => 'The G%'])->fetch()
);
/*
Array
(
    [0] => stdClass Object
        (
            [name] => The Godfather (1972)
            [rating] => 9.2
        )

    [1] => stdClass Object
        (
            [name] => The Godfather: Part II (1974)
            [rating] => 9
        )

    [2] => stdClass Object
        (
            [name] => The Good, the Bad and the Ugly (1966)
            [rating] => 8.9
        )
)
 */

// FETCHING LISTS
// SELECT id AS id , name AS name  FROM movies WHERE name LIKE 'The G%'
print_r(
    $fluentQuery->table('movies')->whereLike('name', 'The G%')->fetchList('id', 'name')
);
/*
Array
(
    [2] => The Godfather (1972)
    [3] => The Godfather: Part II (1974)
    [8] => The Good, the Bad and the Ugly (1966)
)
 */

// If the displayName is set then;
// SELECT id, name FROM movies WHERE name LIKE 'The G%'
print_r(
    $fluentQuery->table('movies')->displayColumn('name')->whereLike('name', 'The G%')->fetchList()
);
/*
Array
(
    [2] => The Godfather (1972)
    [3] => The Godfather: Part II (1974)
    [8] => The Good, the Bad and the Ugly (1966)
)
 */

// GETTING INFORMATION

$tableName = $fluentQuery->table('movies')->getTableName(); // 'movies'
$tableAlias = $fluentQuery->table('movies', 'MyMovies')->getTableAlias(); // 'MyMovies'
$tableName = $fluentQuery->table('movies')->tableAlias('MyMovieAlias')->getTableAlias(); // 'MyMovieAlias'

$displayCol = $fluentQuery->table('movies', 'MyMoviesAlias')->displayColumn('name')->getDisplayColumn(); // 'name'
$displayCol = $fluentQuery->table('movies', 'MyMoviesAlias', 'name')->getDisplayColumn(); // 'name'
unset($tableName, $tableAlias, $displayCol);
// Getting internal objects

$pdo    = $fluentQuery->getPdo();     // The pdo connection
$logger = $fluentQuery->getLogger();  // The instance of psr logger
$cache  = $fluentQuery->getCache();   // The instance of redis-cache pool

unset($pdo, $logger, $cache);
// The return types
$fluentQuery->table('movies')->fetch();
// Returns an array of stdClass (or array of FluentPdoModel in Model mode - see below)

$fluentQuery->table('movies')->fetchOne();
// Returns a stdClass (FluentPdoModel in Model mode - see below)

$fluentQuery->table('movies')->displayColumn('name')->fetchList();
// Returns an array indexed on key -> value pairs ie. [1 => 'Bilbo', 2 => 'Sam']

$fluentQuery->table('movies')->fetchField();
// Returns a single field

$fluentQuery->table('movies')->fetchCallback(function($record) {
    $record->extra_info = 'Some more info.';
    return $record;
});
// Returns an array of mixed after the callback has been applied to the record.
print_r(
    $fluentQuery->table('movies')->whereArr(['name' => 'abc'])->fetchSqlQuery()
);
/*
Array
(
    [0] => SELECT * FROM movies WHERE name = ?
    [1] => Array
        (
            [0] => abc
        )
)
*/

list($sql, $params) = $fluentQuery->table('movies')->whereArr(['name' => 'abc'])->fetchSqlQuery();
print_r(
    $fluentQuery->buildQuery($sql, $params)
);
// SELECT * FROM movies WHERE name = 'abc'

// INSERT, UPDATE, DELETE & UPSERT
$dataObj = (object)[
    'name'              => 'To Kill A Mocking Bird (1970)',
    'rating'            => 8.1,
    'publish_dt'        => '1970-10-10',
    'created_by_id'     => 1,
    'modified_by_id'    => 1,
    'status'            => 1,
];
$movie = $fluentQuery->table('movies')->insert($dataObj);
// INSERT INTO movies (name, rating, publish_dt, created_by_id, modified_by_id, status) VALUES ('To Kill A Mocking Bird (1970)', 8.1, '1970-10-10', 1, 1, 1)
$dataObj = clone($dataObj);
unset($dataObj->id);

$numAffected = $fluentQuery->table('movies')->wherePk($movie->id)->update((object)['rating' => 8.2]);
// UPDATE movies SET rating = 8.2 WHERE id = 11

$rating = $fluentQuery->table('movies')->fetchField('rating', $movie->id);
// SELECT rating FROM movies WHERE id = 11 LIMIT 1
// rating = 8.2

$dataObj->rating = 9.1;
$fluentQuery->upsert($dataObj, ['name']);
//SELECT id FROM movies WHERE name = 'To Kill A Mocking Bird (1970)' LIMIT 1
// if the record matches then:
// UPDATE movies SET name = 'To Kill A Mocking Bird (1970)', rating = 9.1, publish_dt = '1970-10-10', created_by_id = 1, modified_by_id = 1, status = 1 WHERE id = 11
// Otherwise
// INSERT INTO movies (name, rating, publish_dt, created_by_id, modified_by_id, status) VALUES ('To Kill A Mocking Bird (1970)', 9.1, '1970-10-10', 1, 1, 1)

// PRIMARY KEYS
// The default primary key name is 'id'
// You can also use '%_id' and it'll replace it with <table_name>_id
$fluentQuery->primaryKeyName('id')->getPrimaryKeyName();
// returns 'id'

//public function registerEventCallback($event, Closure $fnCallback)

//public function structure($primaryKeyName='id', $foreignKeyName='%s_id')
//public function associations(array $associations)
//public function schema(array $schema)
//public function getColumns($keysOnly=true)
//public function getPrimaryKeyName()



//public function select($columns='*', $alias=null, $addAllIfEmpty=false)
//public function logQueries($logQueries=true)
//public function distinct($distinct=true)
//public function withBelongsTo($with_belongs_to=true)
//public function autoJoin($alias, $type=self::LEFT_JOIN)
//public function having($statement, $operator = self::OPERATOR_AND)
//public function orderBy($columnName, $ordering = '')
//public function groupBy($columnName)
//public function limit($limit, $offset = null)
//public function getLimit()
//public function offset($offset)
//public function join($table, $constraint=null, $table_alias = null, $join_operator = '')
//public function leftJoin($table, $constraint, $table_alias=null)
//public function getSelectQuery()
//public function insert($records)
//public function insertSqlQuery($records)
//public function upsert($data, array $match_on=[])
//public function upsertOne(stdClass $object, array $match_on=[])
//public function update(stdClass $record)
//public function updateSqlQuery($record)
//public function delete($deleteAll=false)
//public function deleteSqlQuery()
//public function count($column='*')
//public function max($column)
//public function min($column)
//public function sum($column)
//public function avg($column)
//public function aggregate($fn)
//public function reset()
//public function slowQuerySeconds($secs)
//public function getNamedWhereIn($field, array $values, $placeholder_prefix=null)

// Return the query for debugging purposes...
$queryArr = $fluentQuery->table('users')->fetchSqlQuery();

/*
Array
(
    [0] => SELECT * FROM users
    [1] => Array
        (
        )

)
*/

// Return one user record (\stdClass)

//$user = $fluentQuery->table('users')->where('username', )