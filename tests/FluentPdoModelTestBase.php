<?php

namespace Terah\FluentPdoModel\Tests;

use Exception;
use PHPUnit_Framework_TestCase;
use Terah\FluentPdoModel\FluentPdoModel;
use Terah\FluentPdoModel\Drivers\AbstractPdo;

abstract class FluentPdoModelTestBase extends PHPUnit_Framework_TestCase
{
    /** @var array|FluentPdoModel[] */
    public array $models = [];

    public ?FluentPdoModel $userModel = null;

    public ?FluentPdoModel $movieModel = null;

    /** @var array|FluentPdoModel[] */
    public array $statements = [];


    public array $db_configs = [
    //CREATE USER tests@'localhost' IDENTIFIED BY 'tests';
    //CREATE DATABASE fluent_pdo_model_tests;
    //GRANT ALL PRIVILEGES ON tests.* TO tests@'localhost';
    //FLUSH PRIVILEGES;
    'testMysql' => [
        //'dsn'       => 'mysql:host=127.0.0.1;port=3306;dbname=tests;charset=utf8',
        'dsn'       => 'mysql:host=127.0.0.1;port=3306;dbname=fluent_pdo_model_tests;charset=utf8',
        'user'      => 'root',
        //'pass'      => 'tests',
    ],
    'testSqlite' => [
        //'dsn'       => 'sqlite:/tmp/fluentpdo-tests.db',
        'dsn'       => 'sqlite::memory:',
    ],
    //CREATE USER postgres;
    //CREATE DATABASE fluent_pdo_model_tests;
    //GRANT ALL PRIVILEGES ON DATABASE fluent_pdo_model_tests TO postgres;
    'testPostgres' => [
        //'dsn'       => 'pgsql:host=localhost;port=5432;dbname=tests;user=tests;password=tests',
        'dsn'       => 'pgsql:host=localhost;port=5432;dbname=fluent_pdo_model_tests',
        'user'      => 'postgres',
    ],
];
    public ?AbstractPdo $sqliteConn = null;

    public ?FluentPdoModel $Statement = null;

    protected function assertValidQueryBuilder($expectedSql, $actualSql, array $expectedParams=[], array $actualParams=[])
    {
        $this->assertEquals($expectedSql, $actualSql);
        $this->assertArraySame($expectedParams, $actualParams);
    }

    protected function assertArraySame(array $expectedParams=[], array $actualParams=[])
    {
        sort($expectedParams);
        sort($actualParams);
        $this->assertEquals(json_encode($expectedParams), json_encode($actualParams));
    }

    protected function assertException(callable $callback, $expectedException = 'Exception', $expectedCode = null, $expectedMessage = null)
    {
        if (!class_exists($expectedException) && !interface_exists($expectedException)) {
            $this->fail("An exception of type '$expectedException' does not exist.");
        }

        try
        {
            $callback();
        }
        catch (Exception $e)
        {
            $class = get_class($e);
            $message = $e->getMessage();
            $code = $e->getCode();

            $extraInfo = $message ? " (message was $message, code was $code)" : ($code ? " (code was $code)" : '');
            $this->assertInstanceOf($expectedException, $e, "Failed asserting the class of exception$extraInfo.");

            if ($expectedCode !== null) {
                $this->assertEquals($expectedCode, $code, "Failed asserting code of thrown $class.");
            }
            if ($expectedMessage !== null) {
                $this->assertContains($expectedMessage, $message, "Failed asserting the message of thrown $class.");
            }
            return;
        }

        $extraInfo = $expectedException !== 'Exception' ? " of type $expectedException" : '';
        $this->fail("Failed asserting that exception$extraInfo was thrown.");
    }

}