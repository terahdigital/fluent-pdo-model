<?php

namespace Terah\FluentPdoModel\Tests;

require_once 'FluentPdoModelTestBase.php';

use DateTime;
use Psr\Log\NullLogger;
use stdClass;
use Terah\FluentPdoModel\ConnectionPool;
use Terah\FluentPdoModel\FluentPdoModel;

class FluentModelTest extends FluentPdoModelTestBase
{
    public FluentPdoModel $FluentPdoModel;

    public function setUp()
    {
        if ( defined('HHVM_VERSION') )
        {
            unset($this->db_configs['testPostgres']);
        }
        foreach ( $this->db_configs as $name => $conf )
        {
            $this->models[$name] = new FluentPdoModel(ConnectionPool::getDriverFromArray($conf, new NullLogger(), new Pool()));
        }
        $this->models['testSqlite']->getPdo()->exec(file_get_contents(__DIR__ . '/test_data.sql'));
        require_once 'UserModel.php';
        require_once 'MovieModel.php';
        $this->userModel    = new UserModel($this->models['testSqlite']->getPdo());
        $this->movieModel   = new MovieModel($this->models['testSqlite']->getPdo());

        $this->FluentPdoModel = $this->models['testSqlite'];
    }

    public function testGetConnection()
    {
        foreach ( $this->statements as $name => $stmt )
        {
            $this->assertInstanceOf('PDO', $stmt->getPdo());
        }
    }

    public function testSetAssociations()
    {
        foreach ( $this->statements as $name => $stmt )
        {
            $stmt->associations([
                'belongsTo'   => [
                    'Group'          => ['groups', 'group_id', 'Group.name', 'group_name'],
                ]
            ]);
            list($sql, $params) = $stmt->autoJoin('Group')->fetchSqlQuery();
            $this->assertEquals('SELECT *, Group.name AS group_name  FROM  LEFT JOIN groups AS Group ON Group.id = .group_id', $sql);
        }
    }

    public function testGetLogger()
    {
        foreach ( $this->statements as $name => $stmt )
        {
            $this->assertInstanceOf('\Psr\Log\AbstractLogger', $stmt->getLogger());
        }
    }

    public function testGetCache()
    {
        foreach ( $this->statements as $name => $stmt )
        {
            $this->assertInstanceOf('\Terah\RedisCache\CacheInterface', $stmt->getCache());
        }
    }

    public function testTable()
    {
        foreach ( $this->statements as $name => $stmt )
        {
            $stmt
                ->table('mytablename', 'MyTableName')
                ->displayColumn('name_field')
                ->primaryKeyName('mycolumn')
                ->where('name', 'qwe')
                ->limit(13, 30)
                ->orderBy('myordercol',);
            $this->assertInstanceOf('Terah\FluentPdoModel\FluentPdoModel', $stmt);
            $this->assertEquals('mytablename', $stmt->getTableName());
            $this->assertEquals('MyTableName', $stmt->getTableAlias());
            $this->assertEquals('name_field', $stmt->getDisplayColumn());
            $this->assertEquals('mycolumn', $stmt->getPrimaryKeyName());
            $this->assertEquals(13, $stmt->getLimit());
            list($act_sql, $act_params) = $stmt->fetchSqlQuery();
            $sql = 'SELECT MyTableName.* FROM mytablename MyTableName WHERE name = ? ORDER BY myordercol ASC LIMIT 13 OFFSET 30';
            $this->assertValidQueryBuilder($sql, $act_sql, ['qwe'], $act_params);
        }
    }

    public function testBadQueryThrowsException()
    {
        $bad_call = function(){
            $this->FluentPdoModel->execute("SELECT a FROM b WHERE a.x = :asdf and a.m = :qwer ", ['asdf' => '123', 'qwef' => 'asdf']);
        };
        $this->assertException($bad_call, 'Exception', null, 'SQLSTATE[HY000]: General error: 1 no such table: b');
        $bad_call = function(){
            $this->FluentPdoModel->query("SELECT a FROM b WHERE a.x = ? and a.m = ? ", ['123', 'asdf']);
        };
        $this->assertException($bad_call, 'Exception', null, 'SQLSTATE[HY000]: General error: 1 no such table: b');
    }

    public function testSimpleSelect()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel->table('movies')->select('id, name')->where('id', 123)->fetchSqlQuery();
        $exp_sql = 'SELECT id, name FROM movies WHERE id = ?';
        $this->assertValidQueryBuilder($exp_sql, $act_sql, [123], $act_params);
    }

    public function testSimpleUpdate()
    {
        $record = [
            'name' => 'Chitty Chitty Bang Bang (1960)',
            'rating' => 8.9
        ];
        list($act_sql, $act_params) = $this->FluentPdoModel->table('movies')->where('id', 1)->updateSqlQuery((object)$record);
        $sql = 'UPDATE movies SET name = ?, rating = ? WHERE id = ?';
        $this->assertValidQueryBuilder($sql, $act_sql, array_merge($record, [1]), $act_params);
    }

    public function testSimpleDelete()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel->table('movies')->where('id', 123)->deleteSqlQuery();
        $sql = 'DELETE FROM movies WHERE id = ?';
        $this->assertValidQueryBuilder($sql,$act_sql, [123], $act_params);
    }

    public function testSimpleDeleteAll()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel->table('movies')->deleteSqlQuery();
        $sql = 'DELETE FROM movies';
        $this->assertValidQueryBuilder($sql,$act_sql, [], $act_params);
    }

    public function testDelete()
    {
        $rowCount = $this->FluentPdoModel->table('movies')->where('id', 1)->delete();
        $this->assertEquals($rowCount, 1);
        $this->assertEquals(9.0, $this->FluentPdoModel->table('movies')->count());
    }

    public function testDeleteAll()
    {
        $rowCount = $this->FluentPdoModel->table('movies')->delete(true);
        $this->assertEquals($rowCount, 10);
        $this->assertEquals(0.0, $this->FluentPdoModel->table('movies')->count());
    }

    public function testDeleteAllFail()
    {
        $bad_query = function()
        {
            $false = $this->FluentPdoModel->table('movies')->delete();
        };
        $this->assertException($bad_query);
        $this->assertEquals(10.0, $this->FluentPdoModel->table('movies')->count());
    }

    public function testSimpleInsert()
    {
        $record = [
            'first_name'    => 'name1',
            'last_name'     => 'last name',
            'city'          => 'city'
        ];
        list($act_sql, $act_params) = $this->FluentPdoModel->table('movies')->insertSqlQuery((object)$record);
        $sql = 'INSERT INTO movies (first_name, last_name, city) VALUES (?, ?, ?)';
        $this->assertValidQueryBuilder($sql, $act_sql, array_values($record), $act_params);
    }

    public function testSimpleUpsert()
    {
        $record = (object)[
            'name'              => 'To Kill A Mocking Bird (1970)',
            'rating'            => 8.1,
            'publish_dt'        => '1970-10-10',
            'created_by_id'     => 1,
            'modified_by_id'    => 1,
            'status'            => 1,
        ];
        $result = $this->FluentPdoModel->table('movies')->upsert($record, ['name']);
        $this->assertEquals($result->name, $record->name);
    }

    public function testUpsertOne()
    {
        $record = (object)[
            'name'          => 'To Kill A Mocking Bird (1970)',
            'rating'        => 8.1,
            'publish_dt'    => '1970-10-10',
            'created_by_id' => 1,
            'modified_by_id' => 1,
            'status'        => 1,
        ];
        $result = $this->FluentPdoModel->table('movies')->upsertOne($record, ['id'], false);
        $this->assertEquals($result->name, $record->name);
    }


    public function testSimpleUpsertById()
    {
        $record = (object)[
            'id'            => 1,
            'name'          => 'To Kill A Mocking Bird (1970)',
            'rating'        => 8.1,
            'publish_dt'    => '1970-10-10',
            'created_by_id' => 1,
            'modified_by_id' => 1,
            'status'        => 1,
        ];
        $result = $this->FluentPdoModel->table('movies')->upsert($record, ['id']);
        $result = $this->FluentPdoModel->table('movies')->fetchOne(1);
        $this->assertEquals($result->name, $record->name);
    }

    public function testSimpleUpsertToInsert()
    {
        $record = (object)[
            'id'            => 11,
            'name'          => 'To Kill A Mocking Bird (1970)',
            'rating'        => 8.1,
            'publish_dt'    => '1970-10-10',
            'created_by_id' => 1,
            'modified_by_id' => 1,
            'status'        => 1,
        ];
        $result = $this->FluentPdoModel->table('movies')->upsert($record, []);
        $result = $this->FluentPdoModel->table('movies')->fetchOne(11);
        $this->assertEquals($result->name, $record->name);
    }

    public function testSlowQueryLogger()
    {
        $hello = $this->FluentPdoModel->logQueries()->slowQuerySeconds(-10)->query("SELECT 'hello' as hello", [])->fetchField('hello');
        $this->assertEquals($hello, 'hello');
    }

    public function testGetNamedWhereIn()
    {
        list($sql, $params) = $this->FluentPdoModel->getNamedWhereIn('name', ['fred', 'betty', 'barney', 'wilma'], 'MyNames');
        $this->assertEquals($sql, 'AND name IN (:MyNames_1,:MyNames_2,:MyNames_3,:MyNames_4)' . PHP_EOL);
        $this->assertEquals(json_encode($params), '{"MyNames_1":"fred","MyNames_2":"betty","MyNames_3":"barney","MyNames_4":"wilma"}');
    }

    public function testGetEmptyNamedWhereIn()
    {
        list($sql, $params) = $this->FluentPdoModel->getNamedWhereIn('name', [], 'MyNames');
        $this->assertEquals($sql, '');
        $this->assertEquals(json_encode($params), '[]');
    }

    public function testMassInsert()
    {
        $record = [
            'name'              => 'To Kill A Mocking Bird (1970)',
            'rating'            => 8.1,
            'publish_dt'        => '1970-10-10',
            'created_by_id'     => 1,
            'modified_by_id'    => 1,
            'status'            => 1,
        ];
        $object     = (object)$record;
        $records    = [$object, $object, $object];
        $rowCount   = $this->FluentPdoModel->table('movies')->insert($records);
        $this->assertEquals($rowCount, 3);
    }

    public function testMassUpsert()
    {
        $record = [
            'name'          => 'To Kill A Mocking Bird (1970)',
            'rating'        => 8.1,
            'publish_dt'    => '1970-10-10',
            'created_by_id' => 1,
            'modified_by_id' => 1,
            'status'        => 1,
        ];
        $object     = (object)$record;
        $records    = [clone($object), clone($object), clone($object)];
        $rowCount   = $this->FluentPdoModel->table('movies')->upsert($records, ['name']);
        $this->assertEquals($rowCount, 3);
    }

    public function testWhere()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel->table('movies')->select('id, name')->where('id', 123)->fetchSqlQuery();
        $sql = 'SELECT id, name FROM movies WHERE id = ?';
        $this->assertValidQueryBuilder($sql,$act_sql, [123], $act_params);
    }
    public function testWhereNot()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel->table('movies')->select('id, name')->whereNot('id', 123)->fetchSqlQuery();
        $sql = 'SELECT id, name FROM movies WHERE id != ?';
        $this->assertValidQueryBuilder($sql,$act_sql, [123], $act_params);
    }
    public function testWhereLike()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel->table('movies')->select('id, name')->whereLike('name', 'jose%')->fetchSqlQuery();
        $sql = 'SELECT id, name FROM movies WHERE name LIKE ?';
        $this->assertValidQueryBuilder($sql,$act_sql, ['jose%'], $act_params);
    }
    public function testWhereNotLike()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel->table('movies')->select('id, name')->whereNotLike('name', 'jose%')->fetchSqlQuery();
        $sql = 'SELECT id, name FROM movies WHERE name NOT LIKE ?';
        $this->assertValidQueryBuilder($sql,$act_sql, ['jose%'], $act_params);
    }
    public function testWhereGt()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel->table('movies')->select('id, name')->whereGt('age', 18)->fetchSqlQuery();
        $sql = 'SELECT id, name FROM movies WHERE age > ?';
        $this->assertValidQueryBuilder($sql,$act_sql, [18], $act_params);
    }
    public function testWhereLt()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel->table('movies')->select('id, name')->whereLt('age', 18)->fetchSqlQuery();
        $sql = 'SELECT id, name FROM movies WHERE age < ?';
        $this->assertValidQueryBuilder($sql,$act_sql, [18], $act_params);
    }
    public function testWhereGte()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel->table('movies')->select('id, name')->whereGte('age', 18)->fetchSqlQuery();
        $sql = 'SELECT id, name FROM movies WHERE age >= ?';
        $this->assertValidQueryBuilder($sql,$act_sql, [18], $act_params);
    }
    public function testWhereLte()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel->table('movies')->select('id, name')->whereLte('age', 18)->fetchSqlQuery();
        $sql = 'SELECT id, name FROM movies WHERE age <= ?';
        $this->assertValidQueryBuilder($sql,$act_sql, [18], $act_params);
    }
    public function testWhereIn()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel->table('movies')->select('id, name')->whereIn('colors', ['blue', 'yellow', 'red'])->fetchSqlQuery();
        $sql = 'SELECT id, name FROM movies WHERE (colors IN (?, ?, ?))';
        $this->assertValidQueryBuilder($sql, $act_sql, ['blue', 'yellow', 'red'], $act_params);
    }
    public function testWhereNotIn()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel->table('movies')->select('id, name')->whereNotIn('colors', ['blue', 'yellow', 'red'])->fetchSqlQuery();
        $sql = 'SELECT id, name FROM movies WHERE (colors NOT IN (?, ?, ?))';
        $this->assertValidQueryBuilder($sql, $act_sql, ['blue', 'yellow', 'red'], $act_params);
    }
    public function testWhereNull()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel->table('movies')->select('id, name')->whereNull('name')->fetchSqlQuery();

        $sql = 'SELECT id, name FROM movies WHERE (name IS NULL)';

        $this->assertValidQueryBuilder($sql, $act_sql, [], $act_params);
    }
    public function testWhereNotNull()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel->table('movies')->select('id, name')->whereNotNull('name')->fetchSqlQuery();
        $sql = 'SELECT id, name FROM movies WHERE (name IS NOT NULL)';
        $this->assertValidQueryBuilder($sql, $act_sql, [], $act_params);
    }

    public function testGetColumns()
    {
        $sqlite     = ConnectionPool::getDriverFromArray($this->db_configs['testSqlite'], new NullLogger(), new Pool());
        $userModel  = new UserModel($sqlite);
        $this->assertArraySame(["comments","email","id","name","status","username"], $userModel->getColumns());
    }

    public function testHaving()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel
            ->table('movies')
            ->select('count(id), name')
            ->groupBy('name')
            ->having('count(id) > 2')
            ->having('max(rating) > 2')
            ->fetchSqlQuery();
        $sql = 'SELECT count(id), name FROM movies GROUP BY name HAVING count(id) > 2 AND max(rating) > 2';
        $this->assertValidQueryBuilder($sql, $act_sql, [], $act_params);
    }

    public function testOrderBy()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel
            ->table('movies')
            ->select('name')
            ->orderBy('name')
            ->fetchSqlQuery();
        $sql = 'SELECT name FROM movies ORDER BY name ASC';
        $this->assertValidQueryBuilder($sql, $act_sql, [], $act_params);
    }

    public function testOffsetLimit()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel
            ->table('movies')
            ->select('name')
            ->orderBy('name')
            ->limit(16, 30)
            ->fetchSqlQuery();
        $sql = 'SELECT name FROM movies ORDER BY name ASC LIMIT 16 OFFSET 30';
        $this->assertValidQueryBuilder($sql, $act_sql, [], $act_params);
    }

    public function testSelect()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel
            ->table('movies')
            ->schema(['name' => 'varchar', 'email' => 'varchar', 'password' => 'varchar'])
            ->select(null)
            ->select()
            ->fetchSqlQuery();
        $sql = 'SELECT name, email, password FROM movies';
        $this->assertValidQueryBuilder($sql, $act_sql, [], $act_params);

        list($act_sql, $act_params) = $this->FluentPdoModel
            ->table('movies')
            ->select(['name', 'email', 'password'])
            ->fetchSqlQuery();
        $sql = 'SELECT name, email, password FROM movies';
        $this->assertValidQueryBuilder($sql, $act_sql, [], $act_params);
    }

    public function testLogQueries()
    {
        $res = $this->FluentPdoModel
            ->logQueries()
            ->table('movies')
            ->select(['name'])
            ->where(['name' => 'I dunno'])
            ->fetch();
        $this->assertEmpty($res);
    }

    public function testAggregates()
    {
        $results = [
            'cnt' => $this->FluentPdoModel->query("SELECT COUNT(rating) as agg FROM movies")->fetchField('agg'),
            'sum' => $this->FluentPdoModel->query("SELECT SUM(rating) as agg FROM movies")->fetchField('agg'),
            'max' => $this->FluentPdoModel->query("SELECT MAX(rating) as agg FROM movies")->fetchField('agg'),
            'min' => $this->FluentPdoModel->query("SELECT MIN(rating) as agg FROM movies")->fetchField('agg'),
            'avg' => $this->FluentPdoModel->query("SELECT AVG(rating) as agg FROM movies")->fetchField('agg'),
        ];
        $this->assertEquals('{"cnt":"10","sum":"89.7","max":"9.3","min":"8.8","avg":"8.97"}', json_encode($results));

        $this->assertEquals($results['cnt'], $this->FluentPdoModel->table('movies')->count('rating'));
        $this->assertEquals($results['sum'], $this->FluentPdoModel->table('movies')->sum('rating'));
        $this->assertEquals($results['max'], $this->FluentPdoModel->table('movies')->max('rating'));
        $this->assertEquals($results['min'], $this->FluentPdoModel->table('movies')->min('rating'));
        $this->assertEquals($results['avg'], $this->FluentPdoModel->table('movies')->avg('rating'));
    }

    public function testNow()
    {
        $this->assertEquals((new DateTime('today'))->format('Y-m-d H:i:s'), $this->FluentPdoModel->NOW('today'));
    }

    public function testJoin()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel
            ->table('movies')
            ->join('users', 'Users.id = Movies.created_by_id', 'Users')
            ->schema(['name' => 'varchar', 'email' => 'varchar', 'password' => 'varchar'])
            ->select(null)
            ->select()
            ->fetchSqlQuery();
        $sql = 'SELECT name, email, password FROM movies JOIN users AS Users ON Users.id = Movies.created_by_id';
        $this->assertValidQueryBuilder($sql, $act_sql, [], $act_params);

        list($act_sql, $act_params) = $this->FluentPdoModel
            ->table('movies')
            ->leftJoin('users', 'Users.id = Movies.created_by_id', 'Users')
            ->schema(['name' => 'varchar', 'email' => 'varchar', 'password' => 'varchar'])
            ->select(null)
            ->select()
            ->fetchSqlQuery();
        $sql = 'SELECT name, email, password FROM movies LEFT JOIN users AS Users ON Users.id = Movies.created_by_id';
        $this->assertValidQueryBuilder($sql, $act_sql, [], $act_params);
    }

    public function testFetchList()
    {
        $list = $this->FluentPdoModel->table('movies')->fetchList('id', 'name');
        $this->assertEquals('{"1":"The Shawshank Redemption (1994)","2":"The Godfather (1972)","3":"The Godfather: Part II (1974)","4":"The Dark Knight (2008)","5":"Pulp Fiction (1994)","6":"12 Angry Men (1957)","7":"Schindler\'s List (1993)","8":"The Good, the Bad and the Ugly (1966)","9":"The Lord of the Rings: The Return of the King (2003)","10":"Fight Club (1999)"}', json_encode($list));
    }


    public function testFetchKeyedOn()
    {
        $records = $this->FluentPdoModel->table('movies')->select(['id', 'name'])->whereGt('rating', 9)->fetch('id');
        $this->assertEquals('{"1":{"id":"1","name":"The Shawshank Redemption (1994)"},"2":{"id":"2","name":"The Godfather (1972)"}}', json_encode($records));
    }

    public function testFetchFieldWithAlias()
    {
        $id = $this->FluentPdoModel->table('movies')->wherePk(1)->fetchField('id as identifier');
        $this->assertEquals(1, $id);
    }

    public function testFetchOne()
    {
        $record = $this->FluentPdoModel->table('movies')->fetchOne(1);
        $this->assertEquals(1, $record->id);
    }

    public function testDistinct()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel
            ->table('movies')
            ->select('name')
            ->distinct()
            ->fetchSqlQuery();
        $sql = 'SELECT DISTINCT name FROM movies';
        $this->assertValidQueryBuilder($sql, $act_sql, [], $act_params);
    }

    public function testSelectDistinct()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel
            ->table('movies')
            ->select('DISTINCT name')
            ->fetchSqlQuery();
        $sql = 'SELECT DISTINCT name FROM movies';
        $this->assertValidQueryBuilder($sql, $act_sql, [], $act_params);
    }

    public function testSelectWithCommas()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel
            ->table('movies', 'Movies')
            ->select(['id, name, rating'])
            ->select('MAX(rating) as maxrate')
            ->fetchSqlQuery();
        $sql = 'SELECT Movies.id, Movies.name, Movies.rating, MAX(rating) as maxrate FROM movies Movies';
        $this->assertValidQueryBuilder($sql, $act_sql, [], $act_params);
    }

    public function testOr()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel
            ->table('movies')
            ->select('name')
            ->whereGt('rating', 9)
            ->_or()
            ->whereLt('rating', 8.9)
            ->fetchSqlQuery();
        $sql = 'SELECT name FROM movies WHERE rating > ? OR rating < ?';
        $this->assertValidQueryBuilder($sql, $act_sql, [9, 8.9], $act_params);
    }

    public function testOrAnd()
    {
        list($act_sql, $act_params) = $this->FluentPdoModel
            ->table('movies')
            ->select('name')
            ->whereGt('rating', 9)
            ->_or()
            ->whereLt('rating', 8.9)
            ->wrap()
            ->_and()
            ->where('rating', 9)
            ->_and()
            ->where('name', 'xyz')
            ->wrap()
            ->_or()
            ->where('name', 'abc')
            ->_and()
            ->wrap()
            ->where('other_field', 15)
            ->fetchSqlQuery();
        $sql = 'SELECT name FROM movies WHERE (rating > ? OR rating < ?) AND (rating = ? AND name = ?) OR (name = ?) AND other_field = ?';
        $this->assertValidQueryBuilder($sql, $act_sql, [9, 8.9, 9, 'xyz', 'abc', 15], $act_params);
    }

    public function testFetchCallback()
    {
        $sqliteConn = $this->FluentPdoModel->getPdo();
        $fnCallback = function(stdClass $record) use ($sqliteConn) {
            $stmt               = new FluentPdoModel($sqliteConn);
            $record->username   = $stmt->table('users')->select('username')->wherePk($record->created_by_id)->fetchField('username');
            return $record;
        };
        $records = $this->FluentPdoModel->table('movies')->select(['name', 'id', 'created_by_id'])->whereGt('rating', 9)->fetchCallback($fnCallback);
        $records = json_encode($records);
        $this->assertEquals($records, '{"1":{"name":"The Shawshank Redemption (1994)","id":"1","created_by_id":"1","username":"admin"},"2":{"name":"The Godfather (1972)","id":"2","created_by_id":"1","username":"admin"}}');
    }

    public function testFetchCallbackTally()
    {
        $fnCallback  = function (stdClass $record)
        {
            return true;
        };
        $num_success = $this->FluentPdoModel->table('movies')
            ->select(['name', 'id', 'created_by_id'])
            ->fetchCallback($fnCallback);
        $this->assertEquals($num_success, 10);
    }

    public function testEmptySkeleton()
    {
        foreach ( $this->models as $name => $model )
        {
            $this->assertEquals([], $model->skeleton());
        }
    }

    public function testEmptyCall()
    {
        $this->assertEquals(1.0, $this->models['testSqlite']->table('users')->count());
    }

    public function testSkeleton()
    {
        $expected = '{"id":null,"username":null,"name":null,"email":null,"comments":null,"status":null}';
        $this->assertEquals($expected, json_encode($this->userModel->skeleton()));
        $expected = '{"id":null,"name":null,"publish_dt":null,"rating":null,"created_by_id":null,"created_ts":null,"modified_by_id":null,"modified_ts":null,"status":null}';
        $this->assertEquals($expected, json_encode($this->movieModel->skeleton()));
    }

    public function testEmptyValidate()
    {
        foreach ( $this->models as $name => $model )
        {
            $bad_query = function() {

                $this->userModel->applyHandlers(new stdClass, FluentPdoModel::SAVE_INSERT);
            };
            $this->assertException($bad_query);
        }
    }

    public function testValidateSuccess()
    {
        $userData = (object)[
            'id'        => 1,
            'username'  => 'fred',
            'name'      => 'Fred Flintstone',
            'email'     => 'fred@flintstones.bed.rock',
            'comments'  => 'He is a bit noisy',
            'status'    => 1,
        ];
        $userData = $this->userModel->applyHandlers($userData, FluentPdoModel::SAVE_INSERT);
        $this->assertNotEmpty($userData);
        $this->assertEmpty($this->userModel->getErrors());
        $movieData = (object)[
            'id'                => 1,
            'name'              => 'Monty Python and the Holy Grail (1975)',
            'publish_dt'        => 'January 1, 1975',
            'rating'            => 7.8,
            'created_by_id'     => 1,
            'created_ts'        => date('Y-m-d H:i:s'),
            'modified_by_id'    => 1,
            'modified_ts'       => date('Y-m-d H:i:s'),
            'status'            => 1,
        ];
        $movieData = $this->movieModel->applyHandlers($movieData, FluentPdoModel::SAVE_INSERT);
        $this->assertNotEmpty($movieData);
        $this->assertEmpty($this->movieModel->getErrors());
    }

    public function testValidateFail()
    {
        $bad_query = function()
        {
            $userData = (object)[
                'id'        => 'abc',
                'username'  => 'fred',
                'name'      => 'Freddy',
                'email'     => 'fred@flintstones.bed.rock',
                'comments'  => 'He is a bit noisy',
                'status'    => 1,
            ];
            $userData = $this->userModel->applyHandlers($userData, FluentPdoModel::SAVE_INSERT);
        };
        $this->assertException($bad_query);

        $bad_query = function()
        {
            $movieData = (object)[
                'id'                => 'abc',
                'name'              => 'Monty Python and the Holy Grail (1975)',
                'publish_dt'        => 'January 1, 1975',
                'rating'            => 9.2,
                'created_by_id'     => 1,
                'created_ts'        => date('Y-m-d H:i:s'),
                'modified_by_id'    => 1,
                'modified_ts'       => date('Y-m-d H:i:s'),
            ];
            $movieData = $this->userModel->applyHandlers($movieData, FluentPdoModel::SAVE_INSERT);
        };
        $this->assertException($bad_query);

    }

    public function testInsert()
    {
        $userData = (object)[
            'username'  => 'fred',
            'name'      => 'Fred Flintstone',
            'email'     => 'fred@flintstones.bed.rock',
            'comments'  => 'He is a bit noisy',
            'status'    => 1,
        ];
        $userData = $this->userModel->insert($userData);
        $this->assertGreaterThan(0, $userData->id);
        $movieData = (object)[
            'name'              => 'Monty Python and the Holy Grail (1975)',
            'publish_dt'        => 'January 1, 1975',
            'rating'            => 9.2,
            'created_by_id'     => 1,
            'created_ts'        => date('Y-m-d H:i:s'),
            'modified_by_id'    => 1,
            'modified_ts'       => date('Y-m-d H:i:s'),
            'status'            => 1,
        ];
        $movieData = $this->movieModel->insert($movieData);
        $this->assertGreaterThan(0, $movieData->id);
    }

    public function testInsertValidationFails()
    {
        $movieData = (object)[
            'name'              => 'Monty Python and the Holy Grail (1975)',
            'publish_dt'        => 'January 1, 1975',
            'rating'            => 'abc',
            'created_by_id'     => 1,
            'created_ts'        => date('Y-m-d H:i:s'),
            'modified_by_id'    => 1,
            'modified_ts'       => date('Y-m-d H:i:s'),
            'status'            => 1,
        ];
        $movieData = $this->movieModel->validationExceptions(false)->insert($movieData);
        $this->assertFalse($movieData);
        $this->assertNotEmpty($this->movieModel->getErrors());
    }

    public function testUpdateValidationFails()
    {
        $movieData = (object)[
            'name'              => 'Monty Python and the Holy Grail (1975)',
            'publish_dt'        => 'January 1, 1975',
            'rating'            => 'abc',
            'created_by_id'     => 1,
            'created_ts'        => date('Y-m-d H:i:s'),
            'modified_by_id'    => 1,
            'modified_ts'       => date('Y-m-d H:i:s'),
            'status'            => 1,
        ];
        $movieData = $this->movieModel->validationExceptions(false)->wherePk(1)->update($movieData);
        $this->assertFalse($movieData);
        $this->assertNotEmpty($this->movieModel->getErrors());
    }

    public function testEmptyBeforeSave()
    {
        $stdClass = new stdClass;
        foreach ( $this->models as $name => $model )
        {
            $this->assertEquals($stdClass, $model->beforeSave($stdClass, FluentPdoModel::SAVE_INSERT));
        }
    }

    public function testBeforeSave()
    {
        $userData = (object)[
            'name'      => 'Fred Flintstone',
            'username'  => 'fred',
            'email'     => 'fred@flintstones.bed.rock',
            'comments'  => 'He is a bit noisy',
            'status'    => 1,
        ];
        $userData = $this->userModel->beforeSave($userData, FluentPdoModel::SAVE_INSERT);
        $this->assertEquals('He likes orange', $userData->comments);
    }

    public function testEmptyAfterSave()
    {
        $stdClass = new stdClass;
        foreach ( $this->models as $name => $model )
        {
            $this->assertEquals($stdClass, $model->afterSave($stdClass, FluentPdoModel::SAVE_INSERT));
        }
    }

    public function testEmptyColumns()
    {
        foreach ( $this->models as $name => $model )
        {
            $this->assertEquals([], $model->getColumns(true));
        }
    }

    public function testFixEmptyTypes()
    {
        $stdClass = new stdClass;
        foreach ( $this->models as $name => $model )
        {
            $this->assertEquals($stdClass, $model->fixTypes($stdClass, FluentPdoModel::SAVE_INSERT));
        }
    }

    public function testAutoJoin()
    {
        $record = $this->movieModel
            ->select('Movie.name')
            ->autoJoin('CreatedBy')
            ->wherePk(1)
            ->fetch();
        $this->assertEquals(json_encode($record), '[{"name":"The Shawshank Redemption (1994)","created_by":"admin"}]');
    }

    public function testWithBelongsTo()
    {
        $record = $this->movieModel
            ->select('Movie.name')
            ->withBelongsTo()
            ->wherePk(1)
            ->fetch();
        $this->assertEquals(json_encode($record), '[{"name":"The Shawshank Redemption (1994)","created_by":"admin","modified_by":"admin"}]');
    }

    public function testJoinFallback()
    {
        $record = $this->movieModel
            ->select('Movie.name')
            ->join('CreatedBy')
            ->wherePk(1)
            ->fetch();
        $this->assertEquals(json_encode($record), '[{"name":"The Shawshank Redemption (1994)","created_by":"admin"}]');
    }

    public function testSelectAllEmpty()
    {
        $record = $this->movieModel
            ->select('Movie.name', null, true)
            ->wherePk(1)
            ->fetch();
        unset($record[0]->created_ts, $record[0]->modified_ts);
        $this->assertEquals(json_encode($record), '[{"id":1,"name":"The Shawshank Redemption (1994)","publish_dt":"1994-10-14","rating":9.3,"created_by_id":1,"modified_by_id":1,"status":1}]');
    }

    public function testPaginate()
    {
        $query  = ['_limit' => 5, '_offset' => 2, '_order' => 'name', '_fields' => 'name|rating', '_search' => 'Fight'];
        list($act_sql, $act_params) = $this->movieModel->filter($query)->paginate($query)->fetchSqlQuery();
        $exp_sql = 'SELECT Movie.name, Movie.rating FROM movies Movie WHERE (1 = ?) AND (Movie.name LIKE ?) ORDER BY Movie.name ASC LIMIT 5 OFFSET 2';
        $this->assertValidQueryBuilder($exp_sql, $act_sql, [1, '%Fight%'], $act_params);
    }

    public function testPaginate2()
    {
        $query  = ['_limit' => 5, '_offset' => 5, '_order' => 'name', '_fields' => 'name|rating|_display_column', 'Movie:rating' => '5.2'];
        list($act_sql, $act_params) = $this->movieModel->filter($query)->paginate($query)->fetchSqlQuery();
        $exp_sql = 'SELECT Movie.name, Movie.rating FROM movies Movie WHERE Movie.rating = ? ORDER BY Movie.name ASC LIMIT 5 OFFSET 5';
        $this->assertValidQueryBuilder($exp_sql, $act_sql, ['5.2'], $act_params);
    }

    public function testPaginate3()
    {
        $query  = ['_limit' => 5, '_order' => 'name', '_fields' => 'name|rating|_display_column', 'Movie:rating' => '5.2'];
        list($act_sql, $act_params) = $this->movieModel->filter($query)->paginate($query)->fetchSqlQuery();
        $exp_sql = 'SELECT Movie.name, Movie.rating FROM movies Movie WHERE Movie.rating = ? ORDER BY Movie.name ASC LIMIT 5';
        $this->assertValidQueryBuilder($exp_sql, $act_sql, ['5.2'], $act_params);
    }

    public function testPaginateWhereClause()
    {
        $query  = ['_limit' => 5, '_offset' => 4, '_order' => 'name', '_fields' => 'name|rating|_display_column', 'Movie:name' => 'whereLike(The%)'];
        list($act_sql, $act_params) = $this->movieModel->filter($query)->paginate($query)->fetchSqlQuery();
        $exp_sql = 'SELECT Movie.name, Movie.rating FROM movies Movie WHERE Movie.name LIKE ? ORDER BY Movie.name ASC LIMIT 5 OFFSET 4';
        $this->assertValidQueryBuilder($exp_sql, $act_sql, ["The%"], $act_params);
    }

    public function testBadPaginate()
    {
        $bad_query = function()
        {
            $query = ['_limit' => 5, '_offset' => 1, '_order' => 'name', '_fields' => 'ABC:name|rating', '_search' => 'Fight', 'ABC:name' => 'Fight'];
            $this->movieModel->filter($query)->paginate($query)->fetchSqlQuery();
        };
        $this->assertException($bad_query);
    }

    public function testBadPaginate2()
    {
        $bad_query = function()
        {
            $query = ['_limit' => 5, '_offset' => 1, '_order' => 'name', '_fields' => 'ABC:name|rating', '_search' => 'Fight', 'ABC:name' => 'Fight'];
            $this->movieModel->paginate($query)->fetchSqlQuery();
        };
        $this->assertException($bad_query);
    }
}