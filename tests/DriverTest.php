<?php

namespace Terah\FluentPdoModel\Tests;

require_once 'FluentPdoModelTestBase.php';

use Psr\Log\NullLogger;
use Terah\FluentPdoModel\ConnectionPool;

class DriverTest extends FluentPdoModelTestBase
{
    public function setUp()
    {
        if ( defined('HHVM_VERSION') )
        {
            unset($this->db_configs['testPostgres']);
        }
    }

    public function testGetLogger()
    {
        foreach ( $this->db_configs as $name => $conf )
        {
            $conn = ConnectionPool::getDriverFromArray($conf, new NullLogger());
            $this->assertInstanceOf('\Psr\Log\AbstractLogger', $conn->getLogger());
        }
    }

    public function testGetCache()
    {
        foreach ( $this->db_configs as $name => $conf )
        {
            $conn = ConnectionPool::getDriverFromArray($conf, new NullLogger());
            $this->assertInstanceOf('\Terah\RedisCache\CacheInterface', $conn->getCache());
        }
    }

    public function testSetValues()
    {
        foreach ( $this->db_configs as $name => $conf )
        {
            $conn = ConnectionPool::getDriverFromArray($conf, new NullLogger());
            $meta = $conn->setConfig(['asdf' => 'asdc'])->getTables();
            $this->assertEmpty($meta);
        }
    }
}