<?php

namespace Terah\FluentPdoModel\Tests;

require_once 'FluentPdoModelTestBase.php';

use PDO;
use Psr\Log\NullLogger;
use Terah\FluentPdoModel\ConnectionPool;

class ConnectionPoolTest extends FluentPdoModelTestBase
{
    public ?ConnectionPool $ConnectionPool = null;

    public function setUp()
    {
        $this->ConnectionPool = ConnectionPool::getInstance();
        if ( defined('HHVM_VERSION') )
        {
            unset($this->db_configs['testPostgres']);
        }
    }

    protected function _fetchCol(PDO $conn, $sql)
    {
        $sth    = $conn->prepare($sql);
        $sth->execute();
        return $sth->fetchColumn();
    }

    public function testAddConnections()
    {
        $this->ConnectionPool->addConnections($this->db_configs, new NullLogger());
        $expected   = 'Testing success!!';
        foreach ( $this->db_configs as $name => $config )
        {
            $testOutput = $this->_fetchCol($this->ConnectionPool->getConnection($name), "SELECT '{$expected}';");
            $this->assertEquals($expected, $testOutput);
        }
    }

    public function testAddConnection()
    {
        $expected   = 'Testing success!!';
        foreach ( $this->db_configs as $name => $config )
        {
            $this->ConnectionPool->addConnection($name, $config, new NullLogger(), new Pool());
            $conn       = $this->ConnectionPool->getConnection($name);
            $testOutput = $this->_fetchCol($conn, "SELECT '{$expected}';");
            $this->assertEquals($expected, $testOutput);
        }
    }

    public function testAdd()
    {
        $expected   = 'Testing success!!';
        foreach ( $this->db_configs as $name => $config )
        {
            $conn       = ConnectionPool::getDriverFromArray($config, new NullLogger(), new Pool());
            ConnectionPool::add($name, $conn);
            $testOutput = $this->_fetchCol(ConnectionPool::get($name), "SELECT '{$expected}';");
            $this->assertEquals($expected, $testOutput);
        }
    }
}